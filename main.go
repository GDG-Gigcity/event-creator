package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"
)

const VERSION = "0.1"

var version, debug, dryrun bool

type Loc struct {
	Where    string `json:"addr"`
	Details  string `json:"loc_details"`
	EmbedURL string `json:"map_embed"`
	MapURL   string `json:"map_url"`
}

type Links struct {
	GooglePlus string `json:"google_plus"`
	Meetup     string `json:"meetup"`
	Video      string `json:"video"`
}

type Event struct {
	Name     string    `json:"event_name"`
	Datetime time.Time `json:"datetime"`
	Loc      Loc       `json:"event_location"`
	Links    Links     `json:"event_links"`
	Details  string    `json:"event_details"`
}

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [arguments] event_directory file\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.BoolVar(&dryrun, "dryrun", false, "Runs the program without writing files")
	flag.BoolVar(&dryrun, "n", false, "Runs the program without writing files")
	flag.BoolVar(&debug, "debug", false, "Runs the program verbosely")
	flag.BoolVar(&debug, "d", false, "Runs the program verbosely")
	flag.BoolVar(&version, "version", false, "Displays the version information and quits")
	flag.BoolVar(&version, "v", false, "Displays the version information and quits")
}

func main() {
	flag.Parse()

	if version {
		fmt.Printf("%s - event generator for GDG Gigcity website\tversion: %s\n", os.Args[0], VERSION)
		os.Exit(0)
	}

	if flag.NArg() < 2 {
		flag.Usage()
		os.Exit(1)
	}

	err := generateJSONFile()
	if err != nil {
		panic(err)
	}
}

func generateJSONFile() error {
	var event Event
	var err error
	fmt.Print("Event name: ")
	event.Name, err = getInput()
	if err != nil {
		return err
	}

	fmt.Print("Date and time of the meeting: ")
	datetime, err := getInput()
	if err != nil {
		return err
	}
	event.Datetime, err = time.Parse("2006-01-02T3:04pm", datetime)
	if err != nil {
		return err
	}

	fmt.Print("Event location: ")
	locations := getLoc()
	loc, err := getInput()
	if err != nil {
		return err
	}
	event.Loc = locations[loc]

	fmt.Print("Google+ URL: ")
	event.Links.GooglePlus, err = getInput()
	if err != nil {
		return err
	}

	fmt.Print("Meetup URL: ")
	event.Links.Meetup, err = getInput()
	if err != nil {
		return err
	}

	fmt.Print("Video URL: ")
	event.Links.Video, err = getInput()
	if err != nil {
		return err
	}

	details, err := ioutil.ReadFile(flag.Arg(1))
	if err != nil {
		return err
	}
	event.Details = string(details)

	j, err := json.Marshal(event)
	if err != nil {
		return err
	}

	f, err := getFileName(flag.Arg(0), event.Name)
	if err != nil {
		return err
	}

	if debug {
		fmt.Println(string(j))
	}

	if !dryrun {
		err = ioutil.WriteFile(path.Join(flag.Arg(0), f), j, 0644)
		if err != nil {
			return err
		}
	}

	return nil
}

func getInput() (string, error) {
	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	if err := s.Err(); err != nil {
		return "", err
	}
	t := s.Text()
	return t, nil
}

func getLoc() map[string]Loc {
	l := make(map[string]Loc)
	l["codej"] = Loc{Where: "100 Cherokee Blvd Chattanooga, TN 37405", Details: "Held in Suite #332 on the 3rd floor, manufacturing side. Offices of Code Journeymen LLC", EmbedURL: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3265.7144199955274!2d-85.31440568442953!3d35.06387837118747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xad2c0d880bb91230!2sBusiness+Development+Center!5e0!3m2!1sen!2sus!4v1450411522838", MapURL: ""}
	return l
}

func getFileName(dir, fileName string) (string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return "", nil
	}

	var fileNames []string
	for _, n := range files {
		fileNames = append(fileNames, n.Name())
	}
	sort.Strings(fileNames)

	var i int
	if fileNames != nil {
		file := fileNames[len(fileNames)-1]
		fs := strings.Split(file, "_")
		if debug {
			fmt.Printf("%#v\n", fs)
		}
		i, err = strconv.Atoi(fs[0])
		if err != nil {
			return "", err
		}
		i++
	}
	fName := strings.ToLower(strings.Replace(fileName, " ", "-", -1))
	fName = fmt.Sprintf("%d_%s", i, fName)

	return fName, nil
}
