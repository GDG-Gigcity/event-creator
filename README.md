# event-creator
Tool to simplify the creation of events for GDG Gigcity website.  This tool is
not meant to be used outside of the [GDG Gigcity
website](https://github.com/GDG-Gigcity/gigcity-site) development process.

## Installing
To build the event creator [Go 1.x](https://golang.org/dl/) is required, using
the latest stable is recommended.  If you already have Go installed and your
`GOPATH` [configured](https://golang.org/doc/install#testing) you can install
this tool by running

    go get github.com/GDG-Gigcity/event-creator

## Usage
To use simply run

    event-creator /path/to/output/directory /path/to/event/details/file

This will ask a few questions about the event and generate a JSON file in the
output directory given when the command was ran.  The output directory is meant
to be the _events_ directory in the GDG website repo so that the site will be
able to parse the file and display the event.  The details file should be a
markdown file containing details about the event in question.  Standard markdown
and a subset of Github flavor syntax is supported by the site, this tool
currently doesn't do any processing on the markdown just copies it.

## TODO
- [x] Add Google Map link to location
- [ ] Add the ability to submit event to Meetup(?)

## Contributing
TODO

## License
event-creator is licensed under the BSD-3 clause license.  See the _LICENSE_
file for details.
